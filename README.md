# Nabil's C++ Practice Repository #

If you are not me, in which you probably are not, feel free to browse through my code since I have most likely encouraged you to do so in my Resume or Cover Letter.

A number of these programs followed the criteria from [this forum post](http://www.cplusplus.com/forum/articles/12974/).

This demonstrates my ability to take a given problem and generate an initial solution and then tweak it to given parameters.

Executables may be found in the Downloads section.