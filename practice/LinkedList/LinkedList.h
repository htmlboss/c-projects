#ifndef LIST_H
#define LIST_H

#include <cstdlib>
#include <iostream>

template<class T> 
class LinkedList {

private:
	typedef struct node {
		T data;
		node* next;
	}* nodePtr;

	nodePtr m_head;
	nodePtr m_curr;
	nodePtr m_temp;
	
public:
	LinkedList();
	~LinkedList();
	 
	//Add Node to list
	void addNode(T Data);

	//Remove Node from list
	void deleteNode(T delData);

	//Print out list contents
	void printList();

	//Reverse order of nodes
	void reverseList();

	//Find middle node and return its data
	T findMiddle(nodePtr head);

	nodePtr getHeadNode() const { if (m_head != NULL) { return m_head; } else std::cout << "List is empty! Add a node first.\n"; }

};

//////////////////
//Implementation//
//////////////////

template <class T>
inline LinkedList<T>::LinkedList() : m_head(nullptr), m_curr(nullptr), m_temp(nullptr) {
}


template <class T>
inline LinkedList<T>::~LinkedList() {
}


template <class T>
inline void LinkedList<T>::addNode(T Data) {

	nodePtr n = new node;
	n->next = nullptr;
	n->data = Data;

	if (m_head != nullptr) {
		m_curr = m_head;

		//advances current until the last pointer
		while (m_curr->next != nullptr) {
			m_curr = m_curr->next;
		}
		m_curr->next = n;
	}
	else {
		//if there is no list made, the first node will be the last node
		m_head = n;
	}
}

template <class T>
inline void LinkedList<T>::deleteNode(T delData) {

	nodePtr delPtr = nullptr;
	m_temp = m_head;
	m_curr = m_head;

	//check if current node is the one pending deletion
	while (m_curr != nullptr && m_curr->data != delData) {
		m_temp = m_curr;
		m_curr = m_curr->next;
	}
	//check condition of while loop
	if (m_curr == nullptr) {
		std::cout << delData << " was not found in the list.\n";
		delete delPtr;
		delPtr = nullptr;
	}
	else {
		delPtr = m_curr;

		//patch hole in list
		m_curr = m_curr->next;
		m_temp->next = m_curr;

		//advance the first value of list, if the initial first value is pending deletion
		if (delPtr == m_head) {
			m_head = m_head->next;
			m_temp = nullptr;
		}

		//delete node
		delete delPtr;
		delPtr = nullptr;

		std::cout << "\nThe value " << delData << " was deleted.\n";
	}

}

template <class T>
inline void LinkedList<T>::printList() {

	//start at beginning of list
	m_curr = m_head;
	while (m_curr != nullptr) {
		std::cout << m_curr->data << std::endl;
		//advance node
		m_curr = m_curr->next;
	}
}

template <class T>
inline void LinkedList<T>::reverseList() {
	
	//If the head-> next pointer is null, the list is already reversed.
	if (m_head == nullptr) {
		return;
	}

	nodePtr m_prev = nullptr, m_next = nullptr;

	m_curr = m_head;
	while (m_curr != nullptr) {
		m_next = m_curr->next;
		m_curr->next = m_prev;
		m_prev = m_curr;
		m_curr = m_next;
	}

	//let m_head point to last node
	m_head = m_prev;

}

template<class T>
inline T LinkedList<T>::findMiddle(nodePtr head) {
	
	if (head->next == NULL) {
		return 0;
	}

	int numNodes = 0;
	nodePtr m_current = head;

	while (m_current->next != NULL) {
		m_current = m_current->next;
		++numNodes;
	}
	++numNodes; // Add 1 since the last node is skipped since ->next == NULL
	
	m_current = head;
	for (int i = 0; i < numNodes / 2; ++i) { //integer division rounds up to the middle node
		m_current = m_current->next;
	}

	return m_current->data;
}

#endif // !LIST_H

