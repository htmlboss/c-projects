// main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "LinkedList.h"

int main() {

	LinkedList<int> myList;
	myList.addNode(5);
	myList.addNode(6);
	myList.addNode(9);
	myList.printList();

	int middle = myList.findMiddle(myList.getHeadNode());

	std::cout << middle;

	system("Pause");
	return 0;
}

