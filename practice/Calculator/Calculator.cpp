// Calculator.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

//returns a vector<float>
std::vector<float> split(const std::string &str, const char &delimiter) {

	std::stringstream ss(str);
	std::string item;
	//New vector for string elements (without operator)
	std::vector<std::string> elements;
	//Convert to vector<float>
	std::vector<float> operands;

	while (std::getline(ss, item, delimiter)) {
		elements.push_back(item);
	}

	operands.reserve(elements.size());
	//Convert <elements> to vector<float>
	for (auto &s : elements) {
		std::stringstream parser(s);
		float x = 0;
		parser >> x;
		operands.push_back(x);
	}
	elements.clear();
	return operands;
}

int main() {

	std::string input;
	std::vector<float> operands;
	float value;

	std::cout << "Enter an expression: "; std::cin >> input;

	if (input.find('+') != std::string::npos) {
		operands = split(input, '+');
		value = operands[0] + operands[1];
		std::cout << "\n" << value << "\n";

	}
	else if (input.find('-') != std::string::npos) {
		operands = split(input, '-');
		value = operands[0] - operands[1];
		std::cout << "\n" << value << "\n";
	}
	else if (input.find('*') != std::string::npos) {
		operands = split(input, '*');
		value = operands[0] * operands[1];
		std::cout << "\n" << value << "\n";
	}
	else if (input.find('/') != std::string::npos) {
		operands = split(input, '/');
		value = operands[0] / operands[1];
		std::cout << "\n" << value << "\n";
	}
	else {
		std::cout << "\nValid operations: '+', '-', '*', '/'" << std::endl;
	}

	system("Pause");
	return 0;
}

