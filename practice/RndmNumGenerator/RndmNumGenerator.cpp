
//Notes:
//Based off of the GCCRandom Class found in Game Coding Complete 4th Edition:
//REMOVED ALL C-STYLE TYPECASTS FOR C++-STYLE.
//MAXINT was changed to INT_MAX found in <cstdlib>

//In C++14, there are much cleaner options to generate pseudo-random numbers...this may be viewed as a multi-compiler solution (or multi-language too :P ).

#include "stdafx.h"

#include <limits>
#include <time.h>
#include <iostream>
#include <cstdlib>

//All these defines would obviously be in their own header. I just have everything here for portability.
#define CMATH_M 397
#define CMATH_N 624
#define CMATH_MATRIX_A 0x9908b0df   /* constant vector A */
#define CMATH_UPPER_MASK 0x80000000 /* most significant w-r bits */
#define CMATH_LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */
#define CMATH_TEMPERING_MASK_B 0x9d2c5680
#define CMATH_TEMPERING_MASK_C 0xefc60000
#define CMATH_TEMPERING_SHIFT_U(y)  (y >> 11)
#define CMATH_TEMPERING_SHIFT_S(y)  (y << 7)
#define CMATH_TEMPERING_SHIFT_T(y)  (y << 15)
#define CMATH_TEMPERING_SHIFT_L(y)  (y >> 18)

//-------------------------------
class CPPRandom {
public:
	CPPRandom();

	unsigned int	Random(unsigned int n);
	double			Random();
	void			SetRandomSeed(unsigned int n);
	unsigned int	GetRandomSeed();
	void			Randomize();

private:
	unsigned int	m_rseed;
	unsigned int	m_rseed_sp;
	unsigned long	m_mt[CMATH_N]; /* the array for the state vector  */
	int				m_mti;		   /* mti==N+1 means mt[N] is not initialized */
};

//-------------------------------
CPPRandom::CPPRandom() {
	m_rseed = 1;
	m_rseed_sp = 0;
	m_mti = CMATH_N + 1;		  /* mti==N+1 means mt[N] is not initialized */
}

//-------------------------------
//Returns a number from 0-n (excludig n)
unsigned int CPPRandom::Random(unsigned int n) {
	
	unsigned long y;
	static unsigned long mag01[2] = { 0x0, CMATH_MATRIX_A };

	if (n == 0) {
		return 0;
	}

	if (m_mti >= CMATH_N) {
		int kk;

		if (m_mti == CMATH_N + 1) { SetRandomSeed(1234); }; //Default seed (1234) is used

		for (kk = 0; kk < CMATH_N - CMATH_M; kk++) {
			y = (m_mt[kk] & CMATH_UPPER_MASK) | (m_mt[kk + 1] & CMATH_LOWER_MASK);
			m_mt[kk] = m_mt[kk + CMATH_M] ^ (y >> 1) ^ mag01[y & 0x1];
		}

		for (; kk < CMATH_N - 1; kk++) {
			y = (m_mt[kk] & CMATH_UPPER_MASK) | (m_mt[kk + 1] & CMATH_LOWER_MASK);
			m_mt[kk] = m_mt[kk + (CMATH_M - CMATH_N)] ^ (y >> 1) ^ mag01[y & 0x1];
		}

		y = (m_mt[CMATH_N - 1] & CMATH_UPPER_MASK) | (m_mt[0] & CMATH_LOWER_MASK);
		m_mt[CMATH_N - 1] = m_mt[CMATH_M - 1] ^ (y >> 1) ^ mag01[y & 0x1];

		m_mti = 0;
	} //if (m_mti >= CMATH_N)

	y = m_mt[m_mti++];
	y ^= CMATH_TEMPERING_SHIFT_U(y);
	y ^= CMATH_TEMPERING_SHIFT_S(y) & CMATH_TEMPERING_MASK_B;
	y ^= CMATH_TEMPERING_SHIFT_T(y) & CMATH_TEMPERING_MASK_C;
	y ^= CMATH_TEMPERING_SHIFT_L(y);


	//Returns a number from range 0-n. Finally!
	return (y%n);
}

//-------------------------------
double CPPRandom::Random() {
	double r = static_cast<double>(Random(INT_MAX));
	double divisor = static_cast<double>(INT_MAX);
	return (r / divisor);
}

//-------------------------------
void CPPRandom::SetRandomSeed(unsigned int n) {
	
	/* 
	setting initial seeds to mt[N] using         
	the generator Line 25 of Table 1 in          
	[KNUTH 1981, The Art of Computer Programming 
	Vol. 2 (2nd Ed.), pp102]
	*/

	m_mt[0] = n & 0xffffffff;
	for (m_mti = 1; m_mti<CMATH_N; m_mti++)
		m_mt[m_mti] = (69069 * m_mt[m_mti - 1]) & 0xffffffff;

	m_rseed = n;
}

//-------------------------------
unsigned int CPPRandom::GetRandomSeed() {
	return m_rseed;
}

//-------------------------------
void CPPRandom::Randomize() {
	time_t seconds = time(NULL);
	SetRandomSeed(static_cast<unsigned int>(seconds));
}


//-------------------------------
int main() {

	CPPRandom r;
	r.Randomize();
	unsigned int num = r.Random(100);
	std::cout << num;

	system("Pause");
	return 0;
}

